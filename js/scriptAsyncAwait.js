import axios from "axios";


let maxEl = 30000

// будущий массив со всеми данными
let reg = []

// объект с данными в виде дерева
let regTree = {}

let tempArr;

//
//

const getData = () => {
  return axios('regions.xml')
    .then(str => (new window.DOMParser()).parseFromString(str.data, "text/html"))
    .then(str => str.getElementsByTagName('row'))
  // .then(str => str)
}


getData().then(res => console.log(res));

async function run() {

  reg = await getData()

  tempArr=[...reg]

  console.log(tempArr);


// tempArr - все ноды полученные по  тегу row
//tempArr.children - все его потомки( там 6 параметров каждого города) код, область код области город...

  for (let i = 0; (i < tempArr.length) && (i < maxEl); i++) {

    reg[i] = {
      obl: tempArr[i]?.children[1].innerText,
      oblCode: tempArr[i]?.children[0].innerText,
      reg: tempArr[i]?.children[3].innerText,
      regCode: tempArr[i]?.children[2].innerText,
      city: tempArr[i]?.children[5].innerText,
      cityCode: tempArr[i]?.children[4].innerText
    }
  }

// Сортировка
  reg.sort((a, b) => (a.oblCode - b.oblCode))



////////////////////////// !!!!!  Самая была долгая часть :  создаем древовидный объект

  for (let i = 0; (i < tempArr.length) && (i < maxEl); i++) {

    // Все работает! загвоздка была в том что нужно уже созданный объект разворачивать регулярно
    // а если такого свойства еще нет, чтобы небыло ошибки, делать проверку есть ли это поле ...
    // это вариант без дополнительных сохранений кода области и кода региона
    //
    //
    // if(regTree[reg[i].obl]&&regTree[reg[i].obl][reg[i].reg]){
    //   regTree[reg[i].obl] = { ...regTree[reg[i].obl],
    //     [reg[i].reg]: { ...regTree[reg[i].obl][reg[i].reg],
    //       [reg[i].cityCode]: reg[i].city
    //     }
    //   }
    // }else{
    //   regTree[reg[i].obl] = { ...regTree[reg[i].obl],
    //     [reg[i].reg]: {
    //       [reg[i].cityCode]: reg[i].city
    //     }
    //   }
    // }


    // все работает!!!

    if (regTree[reg[i].oblCode] && regTree[reg[i].oblCode][reg[i].obl] && regTree[reg[i].oblCode][reg[i].obl][reg[i].regCode]) {
      regTree[reg[i].oblCode] = {
        ...regTree[reg[i].oblCode],
        [reg[i].obl]: {
          ...regTree[reg[i].oblCode][reg[i].obl],
          [reg[i].regCode]: {
            ...regTree[reg[i].oblCode][reg[i].obl][reg[i].regCode],
            [reg[i].reg]: {
              ...regTree[reg[i].oblCode][reg[i].obl][reg[i].regCode][reg[i].reg],
              [reg[i].cityCode]: reg[i].city
            }
          }
        }
      }
    } else if (regTree[reg[i].oblCode]) {
      regTree[reg[i].oblCode] = {
        ...regTree[reg[i].oblCode],
        [reg[i].obl]: {
          ...regTree[reg[i].oblCode][reg[i].obl],
          [reg[i].regCode]: {
            [reg[i].reg]: {
              [reg[i].cityCode]: reg[i].city
            }
          }
        }
      }
    } else {
      regTree[reg[i].oblCode] = {
        ...regTree[reg[i].oblCode],
        [reg[i].obl]: {
          [reg[i].regCode]: {
            [reg[i].reg]: {
              [reg[i].cityCode]: reg[i].city
            }
          }
        }
      }
    }
  }

  console.log(regTree);


  localStorage.setItem('test', JSON.stringify(regTree));


// ==================  сохранять файл , правда поштучно! и с запросом
// var blob = new Blob(["Hello, world!"], {type: "text/plain;charset=utf-8"});
// FileSaver.saveAs(blob, "hello world.txt");

  // console.log(reg);

}


run()









// первый вариант - когда в консольке мы просто отделяем области и города разделителем ======
// logConsole(reg)
function logConsole(arr) {
  let reg = arr
// вывод на экран всех областей раенов городов
  console.log(`==============Область: ${reg[0].obl}`)
  console.log(`=====Раен: ${reg[0].reg}`)


  for (let i = 0; i < reg.length; i++) {
    if ((i > 1) && (reg[i].obl !== reg[i - 1].obl)) {
      console.log(`==============Область: ${reg[i].obl}`)
    }
    if ((i > 1) && (reg[i].reg !== reg[i - 1].reg)) {
      console.log(`=====Раен: ${reg[i].reg}`)
    }
    // раскомментить для вывода городов , убрать чтобы видеть только области и раены
    console.log(`Город: ${reg[i].city}`)
  }
}
